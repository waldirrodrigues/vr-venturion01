﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatePrefab : MonoBehaviour {

    public GameObject prefab;
    public Transform point;
    	
    public void SpawnPrefab()
    {
        Instantiate(prefab, point.position, Quaternion.identity);
    }
}
