﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeBehaviour : MonoBehaviour {

    public int life;

    public void TakeDamage(int dmg)
    {
        life -= dmg;

        if (life <= 0)
        {
            Death();
        }

    }

    void Death()
    {
        Destroy(gameObject);
    }
}
