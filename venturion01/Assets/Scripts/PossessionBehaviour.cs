﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PossessionBehaviour : MonoBehaviour {

    public Transform rayOrigin, initialPos;
    public string tagPossess;
    public float timeToPossess;
    public Vector3 hitPosition, newPosition;

    public RaycastHit hit;
    public bool statueFound, possessed;
    public float time;

    // Use this for initialization
    void Start()    
    {

    }

    // Update is called once per frame
    void Update() {

        if (!possessed)
        {
            if (statueFound)
            {
                time += Time.deltaTime;
            }

            if (time > timeToPossess)
            {
                Possession();
            }

            Debug.DrawRay(rayOrigin.position, rayOrigin.forward, Color.cyan);
            if (Physics.Raycast(rayOrigin.position, rayOrigin.forward, out hit) && hit.transform.tag == tagPossess)
            {
                statueFound = true;
                hitPosition = hit.transform.position;
            }
            else
            {
                statueFound = false;
            }
        }

        else
        {
            if(Vector3.Distance(newPosition, transform.position) > .5f)
            {
                transform.position = initialPos.position;
                possessed = false;
            }
        }
    }

    public void Possession()
    {
        Vector3 newPosition;

        newPosition = new Vector3(hitPosition.x - rayOrigin.position.x, hitPosition.y - rayOrigin.position.y, hitPosition.z - rayOrigin.position.z);

        transform.position = newPosition;

        possessed = true;

        time = 0;
    }
}
