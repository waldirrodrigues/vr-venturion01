﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.IO.Ports;

public class AndroidTest : MonoBehaviour {

    public string COM;
    SerialPort stream;


	// Use this for initialization
	void Start () {

        stream = new SerialPort("\\\\.\\" + COM, 9600);
        stream.ReadTimeout = 50;
        stream.Open();

        Invoke("WriteToArduino", 2f);

	}
	
	// Update is called once per frame
	void Update () {

    }

    public void WriteToArduino(string message)
    {
        stream.WriteLine(message);
        stream.BaseStream.Flush();
    }

    private void OnApplicationQuit()
    {
        WriteToArduino("AQUECEDOROFF");
        WriteToArduino("VENTILADOROFF");
    }
}
