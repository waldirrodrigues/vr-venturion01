﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObjectBehaviour : MonoBehaviour {

    public Vector3 position;
    public float maxDist;
    public List<GameObject> show;
    public bool moved;

    private void SetPosition()
    {
        position = transform.position;
        moved = false;
    }

    // Use this for initialization
    private void OnEnable () {

        moved = true;
        Invoke("SetPosition", .5f);
		
	}
	
	// Update is called once per frame
	void Update () {

        if(!moved)
        {
            if(Vector3.Distance(transform.position, position) > maxDist)
            {
                foreach (GameObject g in show)
                {
                    g.SetActive(true);
                }

                //show.SetActive(true);
                moved = true;
            }
        }

	}
}
