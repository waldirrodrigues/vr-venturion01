﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotController : MonoBehaviour {

    public GameObject projectile;
    public Transform projPos;
    
    public void Shot()
    {
        Instantiate(projectile, projPos.position, projPos.rotation * projectile.transform.rotation);
    }
}
