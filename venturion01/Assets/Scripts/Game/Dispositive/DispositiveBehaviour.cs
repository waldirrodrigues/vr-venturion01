﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DispositiveBehaviour : MonoBehaviour {

    public enum DispositiveStatus
    {
        Offline,
        Discharged,
        Blue,
        Orange,
        Overcharged
    };

    public SteamVR_TrackedController ctrl;
    public ObjectsEvents discharged, blue, orange, overcharged;
    public ushort hapticOffline, hapticDischarged, hapticBlue, hapticOrange, hapticOvercharged;
    public DispositiveStatus status;
    public float totalTimeMin;
    public Text txtTime;

    private float totalTimeSec, timeMin, timeSec, timeCentSec, time;
    public ushort hapticVel;
    public bool haptic;
    
	// Use this for initialization
	void Start ()
    {
        ctrl = FindObjectOfType<SteamVR_TrackedController>();
        status = DispositiveStatus.Offline;
        totalTimeSec = totalTimeMin * 60;
        time = 0;

        timeMin = totalTimeMin;
        DownTime();
	}
	
    public void PlayDischarged()
    {
        discharged.Invoke();
        status = DispositiveStatus.Discharged;
    }

    public void PlayBlue()
    {
        blue.Invoke();
        status = DispositiveStatus.Blue;
    }

    public void PlayOrange()
    {
        orange.Invoke();
        status = DispositiveStatus.Orange;
    }

    public void PlayOvercharged()
    {
        overcharged.Invoke();
        status = DispositiveStatus.Overcharged;
    }

    private void Update()
    {
        Timer();

        Haptic();
    }

    public void Timer()
    {
        time += Time.deltaTime;
        
        if(time >= 1)
        {
            DownTime();
        }

        timeCentSec =(99f - Mathf.Floor(time * 100f));
        txtTime.text = timeMin.ToString("00") + ":" + timeSec.ToString("00");
    }

    public void DownTime()
    {
        timeSec--;
        time = 0;

        if (timeSec < 0)
        {
            timeSec = 59;
            timeMin--;
        }
    }

    public void Trigger()
    {
        if(status == DispositiveStatus.Offline)
        {
            hapticVel = hapticOffline;
            haptic = true;
        }

        else if(status == DispositiveStatus.Blue)
        {
            hapticVel = hapticBlue;
            haptic = true;
        }
        else if (status == DispositiveStatus.Orange)
        {
            hapticVel = hapticOrange;
            haptic = true;
        }
        else if (status == DispositiveStatus.Discharged)
        {
            hapticVel = hapticDischarged;
            haptic = true;
        }
        else if (status == DispositiveStatus.Overcharged)
        {
            hapticVel = hapticOvercharged;
            haptic = true;
        }
    }

    public void Untrigger()
    {
        if(status == DispositiveStatus.Blue)
        {
            haptic = false;
        }
    }

    public void Haptic()
    {
        if(haptic)
            SteamVR_Controller.Input((int)ctrl.controllerIndex).TriggerHapticPulse(hapticVel);
    }
}
