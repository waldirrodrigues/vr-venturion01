﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalCountBehaviour : MonoBehaviour {

    public int qtdCrystal;

    private int crystals;

	// Use this for initialization
	void Start () {

        crystals = 0;
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void CrystalFound()
    {
        crystals++;
    }
}
