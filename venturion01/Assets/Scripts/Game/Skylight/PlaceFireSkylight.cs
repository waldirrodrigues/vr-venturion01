﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceFireSkylight : MonoBehaviour {

    public GameObject sparkleParticle;

    private StatusController status;

    public void Start()
    {
        status = FindObjectOfType<StatusController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(status.status == StatusController.Status.Skylight && other.tag == "Fire")
        {
            sparkleParticle.SetActive(true);
        }
    }
}
