﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SparkleSkylight : MonoBehaviour {

    public Transform[] metalTrail;
    public float speed;
    public BaseEvents events;
    public SkylightBehaviour skylight;
    public float timeToRun;

    private int pos;
    private bool move;
    private float time;

	// Use this for initialization
	void Start ()
    {
        pos = 0;
        move = true;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (move)
        {
            if (time < timeToRun)
            {
                time += Time.deltaTime;
            }

            else
            {
                transform.position = Vector3.MoveTowards(transform.position, metalTrail[pos].position, speed * Time.deltaTime);

                if (Vector3.Distance(transform.position, metalTrail[pos].position) < .01f)
                {
                    pos++;

                    if (pos == metalTrail.Length)
                    {
                        move = false;

                        events.Invoke();

                        skylight.SealOpen();

                        gameObject.SetActive(false);
                    }
                }
            }
        }
	}
}