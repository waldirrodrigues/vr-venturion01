﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkylightBehaviour : MonoBehaviour {

    public int qtdSeals;

    private int openSeals;
    

    public void SealOpen()
    {
        openSeals++;

        VerifySeal();
        
    }

    public void VerifySeal()
    {
        if(openSeals >= qtdSeals)
        {
            gameObject.ChangeStatus(StatusController.Status.Mirror);
        }
    }
}
