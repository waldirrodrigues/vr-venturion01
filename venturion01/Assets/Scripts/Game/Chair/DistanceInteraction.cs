﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceInteraction : MonoBehaviour {

    public Transform obj1, obj2;
    public float dist;
    public BaseEvents eventNext, eventFar;
    public bool next;

	// Use this for initialization
	void Start () {

        next = false;
		
	}
	
	// Update is called once per frame
	void Update () {

        if(!next && Vector3.Distance(obj1.position, obj2.position) < dist)
        {
            next = true;
            eventNext.Invoke();
        }

        else if(next && Vector3.Distance(obj1.position, obj2.position) > dist)
        {
            next = false;
            eventFar.Invoke();
        }
		
	}
}
