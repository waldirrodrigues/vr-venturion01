﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectSit : MonoBehaviour {

    public Transform orig, forward;
    public LayerMask layerOrig, layerForwd;
    public ColliderTrigger col;
    public bool sitted;
    public float timeToEnd;
    public BaseEvents events;

    private float time;

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        if (col.activated)
        {
            Raycast();
        }

        if (sitted)
        {
            time += Time.deltaTime;

            if(time >= timeToEnd)
            {
                gameObject.ChangeStatus(StatusController.Status.Egg);
            }
        }
    }

    public void Raycast()
    {
        RaycastHit hit;

        Debug.DrawRay(orig.position, Vector3.down * 5, Color.blue);

        if(Physics.Raycast(orig.position, Vector3.down, out hit, 10f, layerOrig.value))
        {
            if(hit.transform.tag == "SitDown")
            {
                Debug.DrawRay(forward.position, forward.forward * 10, Color.green);

                if (Physics.Raycast(forward.position, forward.forward, out hit, 10f, layerForwd.value))
                {
                    if (hit.transform.tag == "SitForward")
                    {
                        sitted = true;
                    }

                    else
                    {
                        sitted = false;
                        time = 0;
                    }
                }

                else
                {
                    sitted = false;
                    time = 0;
                }
            }

            else
            {
                sitted = false;
                time = 0;
            }
        }

        else
        {
            sitted = false;
            time = 0;
        }
    }
}
