﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObject : MonoBehaviour {

    public Transform obj, tracker, objRef, initialRef;
    private Vector3 initialPos, initialRot;

    // Use this for initialization
    void Start()
    {
        initialPos = initialRef.position;
        initialRot = initialRef.rotation.eulerAngles;
    }

    // Update is called once per frame
    void Update()
    {
        obj.rotation = Quaternion.Euler(tracker.rotation.eulerAngles);
        obj.position = objRef.position + (tracker.position - initialPos);
    }
}
