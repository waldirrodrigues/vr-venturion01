﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnMonster : MonoBehaviour {

    public GameObject monster;
    public Transform[] spawnPos;
    public float spawnTime;

    private float time;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        time += Time.deltaTime;

        if(time > spawnTime)
        {
            int pos = Random.Range(0, spawnPos.Length);

            Instantiate(monster, new Vector3(Random.Range(-5, 5), Random.Range(-.5f, 2f), Random.Range(-2f, 2f)), spawnPos[pos].rotation);
            time = 0;
        }
	}
}
