﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderTrigger : MonoBehaviour
{
    public ColliderBehaviour[] cols;
    public int qtdCollider;
    public bool activated;
    public GameObject objActivated;
    public BaseEvents activeEvents, deactiveEvents;

    private void Start()
    {
        objActivated.SetActive(false);

        qtdCollider = 0;

        for (int i = 0; i < cols.Length; i++)
        {
            cols[i].col = this;
        }
    }

    private void Update()
    {
        if(qtdCollider == cols.Length)
        {
            activated = true;
            objActivated.SetActive(true);
            activeEvents.Invoke();
        }
    }

    public void ColliderFound()
    {
        qtdCollider++;
    }

    public void ColliderUnfound()
    {
        qtdCollider--;

        activated = false;

        objActivated.SetActive(false);

        deactiveEvents.Invoke();
    }
}
