﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallEvents : MonoBehaviour {

    public BaseEvents events;
    
	public void InvokeEvents()
    {
        events.Invoke();
	}
}
