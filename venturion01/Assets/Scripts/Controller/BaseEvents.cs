﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class BaseEvents
{
    public UnityEvent[] events;
    public AnimTrigger[] animations;

    public virtual void Invoke()
    {
        InvokeEvents();
        InvokeAnimations();
    }

    private void InvokeEvents()
    {
        foreach (UnityEvent e in events)
        {
            e.Invoke();
        }
    }

    private void InvokeAnimations()
    {
        foreach (AnimTrigger a in animations)
        {
            a.SetAnimationTrigger();
        }
    }
}

[Serializable]
public class ObjectsEvents : BaseEvents
{
    public GameObject[] objects;

    public override void Invoke()
    {
        base.Invoke();
    }

    public void InvokeObjects()
    {
        foreach (GameObject g in objects)
        {
            g.SetActive(!g.activeSelf);
        }
    }
}