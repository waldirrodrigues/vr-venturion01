﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ColliderBehaviour : MonoBehaviour {
    
    public ColliderTrigger col;

    public string objTag;
    
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == objTag)
        {
            col.ColliderFound();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == objTag)
        {
            col.ColliderUnfound();
        }
    }
}
