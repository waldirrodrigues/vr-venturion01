﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TipsController : MonoBehaviour {
    
    public Tip[] tips;

    private StatusController status;

	// Use this for initialization
	void Start () {

        status = GetComponent<StatusController>();
	}

    public void ShowTips()
    {

        foreach (Tip t in tips)
        {
            if(t.status == status.status)
            {
                t.PlayTip();
            }
        }
    }

}

[Serializable]
public class Tip
{
    public string tag;
    public StatusController.Status status;
    public TipLevel[] levels;
    private int lvl;

    public void PlayTip()
    {
        levels[lvl].Invoke();
    }

    private void ChangeLvl()
    {
        if (lvl < levels.Length - 1)
            lvl++;

    }
}

[Serializable]
public class TipLevel
{
    public UnityEvent[] events;
    public AnimTrigger[] animations;
    public SoundWithSource[] sounds;

    public void Invoke()
    {
        InvokeEvents();
        InvokeAnimations();
        InvokeSound();
    }

    private void InvokeEvents()
    {
        if (events != null)
        {
            foreach (UnityEvent e in events)
            {
                e.Invoke();
            }
        }
    }

    private void InvokeAnimations()
    {
        if (animations != null)
        {
            foreach (AnimTrigger a in animations)
            {
                a.SetAnimationTrigger();
            }
        }
    }

    private void InvokeSound()
    {
        if (sounds != null)
        {
            foreach (SoundWithSource s in sounds)
            {
                s.PlaySound();
            }
        }
    }
}

[Serializable]
public class SoundWithSource
{
    public AudioSource source;
    public AudioClip[] clips;

    public void PlaySound()
    {
        source.clip = clips[UnityEngine.Random.Range(0, clips.Length)];
        source.Play();
    }
}
