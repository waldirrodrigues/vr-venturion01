﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastTrigger : TriggerBehaviour {

    public Raycasts[] rays;
    public LayerMask raycastMask;
    public StatusController.Status status;

    private StatusController gameStatus;

    private void Start()
    {
        gameStatus = FindObjectOfType<StatusController>();
    }

    void FixedUpdate ()
    {
        if(gameStatus.status == status)
        {
            PlayRaycasts();
        }
	}

    public void PlayRaycasts()
    {
        foreach (Raycasts r in rays)
        {
            if(!r.SearchObj(raycastMask.value))
            {
                return;
            }
        }

        Invoke();
    }
}

[Serializable]
public class Raycasts
{
    public Transform origin;
    public string rayTag;

    public bool SearchObj(int mask)
    {
        RaycastHit hit;

        Debug.DrawRay(origin.position, origin.forward, Color.blue);

        if (Physics.Raycast(origin.position, origin.forward, out hit, 10f, mask))
        {
            if (hit.transform.tag == rayTag)
            {
                return true;
            }
        }

        return false;
    }
}
