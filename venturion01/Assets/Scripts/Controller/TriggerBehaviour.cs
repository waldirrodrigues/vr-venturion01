﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerBehaviour : MonoBehaviour {

    public UnityEvent[] events;
    public AnimTrigger[] animations;
    
    public void Invoke()
    {
        InvokeEvents();
        InvokeAnimations();
    }

    private void InvokeEvents()
    {
        foreach (UnityEvent e in events)
        {
            e.Invoke();
        }
    }

    private void InvokeAnimations()
    {
        foreach (AnimTrigger a in animations)
        {
            a.SetAnimationTrigger();
        }
    }

}
