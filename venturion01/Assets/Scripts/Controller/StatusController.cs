﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusController : MonoBehaviour {

    [Serializable]
    public enum Status
    {
        Fire = 0,
        Skylight = 1,
        Mirror = 2,
        Throne = 3,
        Egg = 4,
        Up = 5,
        Dragon = 6,
        End = 7
    };
    public Status status;
    public StatusEvents[] events;

    private void Start()
    {
        InvokeEventStatus();
    }


    public void ChangeStatus(Status stats)
    {
        status = stats;

        InvokeEventStatus();
    }
    public void ChangeStatus(int stats)
    {
        status = (Status)stats;

        InvokeEventStatus();
    }

    public void InvokeEventStatus()
    {
        foreach (StatusEvents s in events)
        {
            if (s.status == status)
            {
                s.InvokeEvents();
            }
        }
    }
}

[Serializable]
public class StatusEvents
{
    public StatusController.Status status;
    public BaseEvents events;

    public void InvokeEvents()
    {
        events.Invoke();
    }
}

public static class ExtendChangeStatus
{
    public static void ChangeStatus(this GameObject go, StatusController.Status stats)
    {
        GameObject.FindObjectOfType<StatusController>().ChangeStatus(stats);
    }
}
