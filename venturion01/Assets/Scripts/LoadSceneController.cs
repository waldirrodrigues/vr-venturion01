﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneController : MonoBehaviour {

    public List<GameObject> lights;

	// Use this for initialization
	void Start () {

        foreach (GameObject g in lights)
        {
            g.SetActive(false);
        }

        Invoke("Load", 1f);
        Load();
	}

    public void Load()
    {

        SceneManager.LoadScene("testBridge");
    }
	
}
