﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MirrorBehaviour : MonoBehaviour {

    #region Old
    //   public Transform sun;
    //   public GameObject spotlight;
    //   public AltarPuzzle altar;
    //   public GameObject sunLight;

    //   private bool sunChanged;

    //void Update () {

    //       RaycastHit hit;

    //       if(Physics.Raycast(sun.position, Vector3.down, out hit) && hit.transform.tag == "Mirror")
    //       {
    //           spotlight.SetActive(true);
    //           spotlight.transform.position = hit.point;
    //       }

    //       else
    //       {
    //           spotlight.SetActive(false);
    //       }

    //       if(Physics.Raycast(spotlight.transform.position, spotlight.transform.forward, out hit)  && hit.transform.tag == "Sun")
    //       {
    //           if (!sunChanged)
    //           {
    //               altar.ChangeSun();
    //               sunChanged = true;
    //               sunLight.SetActive(true);
    //           }
    //       }

    //       else if(sunChanged)
    //       {
    //           altar.ChangeSun();
    //           sunChanged = false;
    //           sunLight.SetActive(false);
    //       }
    //}

    #endregion

    public MirrorAct[] actions;
    public LayerMask mask;
    public Transform sun;
    public GameObject spotlight, spotlightForward, partObj, spotObj;
    public Light lightToChange;
    public float min, max;
    public ParticleSystem particles;

    private void FixedUpdate()
    {
        RaycastHit hit;
        Ray r = new Ray(sun.position, sun.transform.forward);

        Debug.DrawRay(r.origin, r.direction * 10, Color.blue);

        if (Physics.Raycast(r, out hit, 10f, mask.value) && hit.transform.tag == "Mirror")
        {
            spotlight.SetActive(true);
            partObj.SetActive(true);
            spotObj.transform.position = hit.point;
            spotlightForward.transform.position = hit.point;
            //spotlight.transform.LookAt(sun.position);
            Vector3 reflectedRay = Vector3.Reflect(sun.position, spotlightForward.transform.right);
            Vector3 reflectedRay2 = Vector3.Reflect(sun.position, spotlightForward.transform.up);

            //Debug.Log(reflectedRay);
            //Debug.Log(reflectedRay2);

            Vector3 final = reflectedRay + reflectedRay2;

            Debug.DrawLine(spotObj.transform.position, final);

            spotObj.transform.LookAt(final);
            Debug.Log(Vector3.Distance(hit.point, hit.transform.position));
            ChangeLight(Vector3.Distance(hit.point, hit.transform.position));
        }

        else
        {
            particles.startColor = new Color(particles.startColor.r, particles.startColor.g, particles.startColor.b, 2.0f / 256.0f);
            partObj.SetActive(false);
            spotlight.SetActive(false);
        }

        //if(Physics.Raycast(spotlight.transform.position, spotlight.transform.forward, out hit))
        //{
        //    foreach (MirrorAct a in actions)
        //    {
        //        if(hit.transform.tag == a.tag)
        //        {
        //            a.anim.SetTrigger(a.trigger);
        //            if(a.call != null)
        //            {
        //                a.call.Invoke();
        //            }
        //        }
        //    }
        //}
    }

    public void ChangeLight(float distance)
    {
        if (distance > min)
        {
            float value = 1 - distance / max;
            lightToChange.intensity = 2 * value;
            particles.startColor = new Color(particles.startColor.r, particles.startColor.g, particles.startColor.b, value / 256.0f);
        }
        else
        {
            particles.startColor = new Color(particles.startColor.r, particles.startColor.g, particles.startColor.b, 2.0f / 256.0f);
        }
    }
}

[Serializable]
public class MirrorAct
{
    public string tag, trigger;
    public Animator anim;
    public UnityEvent call;
}
