﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AltarHoleBehaviour : MonoBehaviour {

    public bool scepter;
    public GameObject lightObj;
    public AltarPuzzle altar;
    public TorchBehaviour torch;
    public bool activated;
    
    private void OnTriggerEnter(Collider other)
    {
        if(scepter && other.tag == "Scepter")
        {
            altar.ChangeScepter();
            DeActivate();
        }

        else if(!scepter && other.tag == "Torch" && torch.fire)
        {
                altar.ChangeTorch();
                DeActivate();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (scepter && other.tag == "Scepter")
        {
            altar.ChangeScepter();
            DeActivate();
        }

        else if (!scepter && other.tag == "Torch" && torch.fire)
        {
            altar.ChangeTorch();
            DeActivate();
        }
    }

    public void DeActivate()
    {
        activated = !activated;
        lightObj.SetActive(activated);
    }
}
