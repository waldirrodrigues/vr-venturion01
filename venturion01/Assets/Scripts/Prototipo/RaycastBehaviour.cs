﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RaycastBehaviour : MonoBehaviour {

    public StatusController.Status statusToWork;
    public Transform raycastOrigin;
    public LayerMask layer;
    public RaycastAction[] rayActions;

    private StatusController statusScript;
    
    // Use this for initialization
    void Start () {

        statusScript = FindObjectOfType<StatusController>();
		
	}
	
	// Update is called once per frame
	void Update () {

        RaycastHit hit;
        if (statusScript.status == statusToWork)
        {
            Debug.DrawRay(raycastOrigin.position, raycastOrigin.forward * 10f, Color.red);

            if (Physics.Raycast(raycastOrigin.position, raycastOrigin.forward, out hit, 10f, layer.value))
            {
                Raycast(hit.transform.tag);

            }
        }
	}

    public void Raycast(string tag)
    {
        foreach (RaycastAction r in rayActions)
        {
            Debug.Log(r.tag);
            if(!r.setRaycast && r.tag == tag)
            {
                r.InvokeEvents();
                r.InvokeAnimations();
                r.setRaycast = true;
            }
        }
    }
}

[Serializable]
public class RaycastAction
{
    public string tag;
    public UnityEvent[] events;
    public AnimTrigger[] animations;
    public bool setRaycast;

    public void InvokeEvents()
    {
        foreach (UnityEvent e in events)
        {
            e.Invoke();
        }
    }

    public void InvokeAnimations()
    {
        foreach (AnimTrigger a in animations)
        {
            a.SetAnimationTrigger();
        }
    }
}
