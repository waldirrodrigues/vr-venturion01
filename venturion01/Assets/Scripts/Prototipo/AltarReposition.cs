﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AltarReposition : MonoBehaviour {

    public float angle = 5.98f;
    public Transform newPos, right, left;
    public bool autoRepos;
    public float timeToAuto;

    private float time;

    private void Start()
    {
        time = 0;
    }

    // Update is called once per frame
    void Update () {

        if(autoRepos)
        {
            time += Time.deltaTime;

            if(time > timeToAuto)
            {
                Reposition();
                time = 0;
            }
        }

        if(Input.GetKeyDown(KeyCode.Z))
        {
            Reposition();
        }                                                               
		
	}

    public void Reposition()
    {
        //transform.position += newPos.position - right.position;
        transform.position += newPos.position - left.position;

        Vector3 posRight, posLeft;

        posRight = right.position;
        posLeft = left.position;

        //Vector3 pos = new Vector3(posLeft.x - posRight.x, posLeft.z- posRight.z);
        Vector3 pos = new Vector3(posRight.x - posLeft.x, posRight.z - posLeft.z);

        Debug.Log(pos);

        float newAngle = Mathf.Atan2(Mathf.Abs(pos.y), Mathf.Abs(pos.x)) * Mathf.Rad2Deg;

        Debug.Log(newAngle);

        newAngle = angle - newAngle;

        Debug.Log(newAngle);

        transform.rotation = Quaternion.Euler(0, transform.eulerAngles.y - newAngle, 0);

        pos = new Vector3(posLeft.x - posRight.x, posLeft.z - posRight.z);

        newAngle = Mathf.Atan2(Mathf.Abs(pos.y), Mathf.Abs(pos.x)) * Mathf.Rad2Deg;

        Debug.Log(newAngle);
    }
}
