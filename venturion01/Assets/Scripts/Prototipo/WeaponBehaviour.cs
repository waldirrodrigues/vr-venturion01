﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponBehaviour : MonoBehaviour {

    public Transform raycastOrig;
    public string enemyTag;
    public Animator anim;
    public GameObject bulletPrefab;

	// Use this for initialization
	void Start ()
    {
        
	}
	
	// Update is called once per frame
	void Update ()
    {
        Debug.DrawRay(raycastOrig.position, -raycastOrig.forward * 10);
	}

    public void Shot()
    {
        RaycastHit hit;

        anim.SetTrigger("Shot");
        Instantiate(bulletPrefab, raycastOrig.position, raycastOrig.rotation * Quaternion.Euler(0, 180, 0));

        if (Physics.Raycast(raycastOrig.position, -raycastOrig.forward, out hit))
        {
            if(hit.transform.tag == enemyTag)
            {
                Destroy(hit.transform.gameObject);
            }
        }
    }
}
