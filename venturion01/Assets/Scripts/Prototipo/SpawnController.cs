﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour {

    public Transform spawnPos;
    public GameObject spawnObj;
    public int qtdSpawn;
    public float timeSpawn;
    public bool startSpawn;

    private float time;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(startSpawn && qtdSpawn > 0)
        {
            time += Time.deltaTime;

            if(time > timeSpawn)
            {
                time = 0;
                SpawnObject();
            }
        }
	}

    public void SpawnObject()
    {
        Instantiate(spawnObj, Vector3.zero, Quaternion.identity);
        qtdSpawn--;
    }

    public void StartSpawn()
    {
        startSpawn = true;
    }
}
