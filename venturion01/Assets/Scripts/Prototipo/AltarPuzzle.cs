﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AltarPuzzle : MonoBehaviour {

    public bool torch, scepter, sun;
    public Transform bridge, bridgeAltar, chair, chairAltar, eggAltar, eggAltarPos;
    public float bridgeSpeed;
    
    public bool mirrorBridge, eggAltarMove;
    
	void Update () {

        if(mirrorBridge)
        {
            if(Vector3.Distance(bridge.position, bridgeAltar.position) > 0.001f)
            {
                bridge.position = Vector3.Lerp(bridge.position, bridgeAltar.position, Time.deltaTime * bridgeSpeed);
                chair.position = Vector3.Lerp(chair.position, chairAltar.position, Time.deltaTime * bridgeSpeed);
            }
        }

        if (eggAltarMove)
        {
            if(Vector3.Distance(eggAltar.position, eggAltarPos.position) > 0.001f)
            {
                eggAltar.position = Vector3.Lerp(eggAltar.position, eggAltarPos.position, Time.deltaTime * bridgeSpeed);
            }
        }
	}

    public void ChangeTorch()
    {
        torch = !torch;

        VerifyPuzzle();
    }

    public void ChangeScepter()
    {
        scepter = !scepter;
        VerifyPuzzle();
    }

    public void ChangeSun()
    {
        sun = !sun;
        VerifyPuzzle();
    }

    public void VerifyPuzzle()
    {
        if(torch && scepter && !sun && !mirrorBridge)
        {
            mirrorBridge = true;
        }

        if(torch && scepter && sun)
        {
            eggAltarMove = true;
        }
    }
}
