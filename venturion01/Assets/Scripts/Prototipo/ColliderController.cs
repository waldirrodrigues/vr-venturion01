﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
public class ColliderController : MonoBehaviour {

    public string tagCollider;
    public UnityEvent[] events;
    public AnimTrigger[] animations;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == tagCollider)
        {
            foreach (UnityEvent e in events)
            {
                e.Invoke();
            }

            foreach (AnimTrigger a in animations)
            {
                a.SetAnimationTrigger();
            }
        }
    }
}
