﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllignBehaviour : MonoBehaviour {

    public Transform bStation;
    public Vector3 basePos;
    public SteamVR_TrackedObject allign;
    public Transform pivotBase, pivotLight, parentPlayer, parentLight, player;

    private void Start()
    {
        if (Vector3.Distance(bStation.position, basePos) > .005f)
        {
            StartCoroutine(FoundBase());
        }
    }

    // Use this for initialization
    IEnumerator FoundBase () {

        for (int i = 1; i < 15; i++)
        {

            allign.index = (SteamVR_TrackedObject.EIndex)i;
            
            Allign();

            yield return new WaitForSeconds(1f);

            Debug.Log(Vector3.Distance(bStation.position, basePos));

            if (Vector3.Distance(bStation.position, basePos) < .005f)
            {
                i = 100;
                break;
            }
        }
    }
	
	// Update is called once per frame
	void Update () {


        if (Vector3.Distance(pivotBase.position, pivotLight.position) > .005f || Input.GetKeyDown(KeyCode.M))
        {
            //Debug.Log(Vector3.Distance(pivotBase.position, pivotLight.position));
            Allign();
            Debug.Log("Alinhou");
        }
		
	}

    public void Allign()
    {
        pivotLight.SetParent(parentPlayer);
        player.SetParent(pivotLight);
        pivotLight.position = pivotBase.position;
        pivotLight.rotation = pivotBase.rotation;
        player.SetParent(parentPlayer);
        pivotLight.SetParent(parentLight);
    }
}
