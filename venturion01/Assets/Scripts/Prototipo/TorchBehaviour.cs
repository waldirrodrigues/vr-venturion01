﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchBehaviour : MonoBehaviour {

    public bool fire, canFire;
    public float timeToTakeFire;
    public GameObject fireLight, fireObj;
    //public ParticleSystem fireObj;

    //private ParticleSystem.EmissionModule fireParticle;
    private float time;
    private Animator anim;

	// Use this for initialization
	void Start () {
        
        //fireParticle = fireObj.emission;
	}
	
	// Update is called once per frame
	void Update () {

        //Debug.DrawRay(fireObj.transform.position, Vector3.back*10, Color.cyan);

        //RaycastHit hit;
        //if(fire && Physics.Raycast(fireObj.transform.position, Vector3.back, out hit, 10f))
        //{
        //    if(hit.transform.tag == "Wind" && fire)
        //    {
        //        fire = false;
        //        fireParticle.rateOverTime = 0;
        //        fireLight.SetActive(false);
        //    }

        //}

        if (canFire && !fire)
        {
            time += Time.deltaTime;

            if (time > timeToTakeFire)
            {
                fireLight.SetActive(true);
                fireObj.SetActive(true);
                fire = true;
                gameObject.ChangeStatus(StatusController.Status.Skylight);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Fire" && !fire)
        {
            canFire = true;
        }

        //if(other.tag == "Wind" && fire)
        //{
        //    fire = false;
        //    fireParticle.rateOverTime = 0;
        //    fireLight.SetActive(false);
        //}
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Fire")
        {
            canFire = false;
        }
    }

    public void PutFire()
    {
        anim.SetTrigger("Fire");
    }
}
