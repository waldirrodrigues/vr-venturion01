﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameController : MonoBehaviour {

    public KeyAction[] keys;
    
	// Update is called once per frame
	void Update () {

        foreach (KeyAction k in keys)
        {
            if(Input.GetKeyDown(k.key))
            {
                k.InvokeEvents();
                k.InvokeAnimations();
                //if (k.events != null)
                //{
                //    foreach (UnityEvent e in k.events)
                //    {
                //        e.Invoke();
                //    }
                //}

                //foreach (AnimTrigger a in k.animations)
                //{
                //    a.anim.SetTrigger(a.trigger);
                //}
            }
        }
	}
}

[Serializable]
public class KeyAction
{
    public string tag;
    public KeyCode key;
    public UnityEvent[] events;
    public AnimTrigger[] animations;

    public void InvokeEvents()
    {
        foreach (UnityEvent e in events)
        {
            e.Invoke();
        }
    }

    public void InvokeAnimations()
    {
        foreach (AnimTrigger a in animations)
        {
            a.SetAnimationTrigger();
        }
    }
}

[Serializable]
public class AnimTrigger
{
    public string trigger;
    public Animator anim;

    public void SetAnimationTrigger()
    {
        anim.SetTrigger(trigger);
    }

}
