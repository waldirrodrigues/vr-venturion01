﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoReposition : MonoBehaviour
{

    public float angleZ, angleY;
    public Transform newPos, right, left;
    public bool autoRepos;
    public float timeToAuto;
    public Vector3 lighthouseRight, lighthouseLeft;
    public Transform lighthouseRObj, lighthouseLObj;
    public Transform player;

    private float time;
    
    private void Start()
    {
        time = 0;
    }

    // Update is called once per frame
    void Update()
    {

        if (autoRepos)
        {
            time += Time.deltaTime;

            if (time > timeToAuto)
            {
                Reposition();
                time = 0;
            }
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            Reposition();
        }

    }

    public void Reposition()
    {
        transform.position += newPos.position - right.position;
        //transform.position += newPos.position - left.position;

        Vector3 posRight, posLeft;

        posRight = right.position;
        posLeft = left.position;

#region Pos Z

        //Vector3 pos = new Vector3(posLeft.x - posRight.x, posLeft.z - posRight.z);
        Vector3 pos = new Vector3(posRight.x - posLeft.x, posRight.z - posLeft.z);

        //Debug.Log(pos);

        float newAngle = Mathf.Atan2(Mathf.Abs(pos.y), Mathf.Abs(pos.x)) * Mathf.Rad2Deg;

        Debug.Log(newAngle);

        newAngle = angleZ - newAngle;

        Debug.Log(newAngle);

        transform.localRotation = Quaternion.Euler(transform.localEulerAngles.x, transform.localEulerAngles.y, transform.localEulerAngles.z + newAngle);

        pos = new Vector3(posLeft.x - posRight.x, posLeft.z - posRight.z);

        newAngle = Mathf.Atan2(Mathf.Abs(pos.y), Mathf.Abs(pos.x)) * Mathf.Rad2Deg;

        Debug.Log(newAngle);

        #endregion


        #region Pos Y

        //Vector3 pos = new Vector3(posLeft.x - posRight.x, posLeft.z - posRight.z);
        pos = new Vector3(posRight.x - posLeft.x, posRight.y - posLeft.y);

        //Debug.Log(pos);

        newAngle = Mathf.Atan2(Mathf.Abs(pos.y), Mathf.Abs(pos.x)) * Mathf.Rad2Deg;

        Debug.Log(newAngle);

        newAngle = angleY - newAngle;

        Debug.Log(newAngle);

        transform.localRotation = Quaternion.Euler(transform.localEulerAngles.x, transform.localEulerAngles.y + newAngle, transform.localEulerAngles.z);

        pos = new Vector3(posLeft.x - posRight.x, posLeft.y - posRight.y);

        newAngle = Mathf.Atan2(Mathf.Abs(pos.y), Mathf.Abs(pos.x)) * Mathf.Rad2Deg;

        Debug.Log(newAngle);

        #endregion
    }

    public float GetZAngle()
    {
        //transform.position += newPos.position - right.position;
        //transform.position += newPos.position - left.position;

        Vector3 posRight, posLeft;

        posRight = right.position;
        posLeft = left.position;

        //Vector3 pos = new Vector3(posLeft.x - posRight.x, posLeft.z - posRight.z);
        Vector3 pos = new Vector3(posRight.x - posLeft.x, posRight.z - posLeft.z);

        //Debug.Log(pos);

        float newAngle = Mathf.Atan2(Mathf.Abs(pos.y), Mathf.Abs(pos.x)) * Mathf.Rad2Deg;

        return newAngle;
    }
    public float GetYAngle()
    {
        //transform.position += newPos.position - right.position;
        //transform.position += newPos.position - left.position;

        Vector3 posRight, posLeft;

        posRight = right.position;
        posLeft = left.position;

        //Vector3 pos = new Vector3(posLeft.x - posRight.x, posLeft.z - posRight.z);
        Vector3 pos = new Vector3(posRight.x - posLeft.x, posRight.y - posLeft.y);

        //Debug.Log(pos);

        float newAngle = Mathf.Atan2(Mathf.Abs(pos.y), Mathf.Abs(pos.x)) * Mathf.Rad2Deg;

        return newAngle;
    }
}
