﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BugSeekBehaviour : MonoBehaviour {

    public Transform target;
    public float moveSpeed = 6, minDistance = 5, rotationSpeed = 1;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        Seek();
    }

    public void Seek()
    {

        Vector3 direction = target.position - transform.position;

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);

        //transform.LookAt(player);

        if (direction.magnitude > minDistance)
        {

            Vector3 moveVector = direction.normalized * moveSpeed * Time.deltaTime;

            //transform.position += moveVector;

            transform.position = Vector3.Lerp(transform.position, transform.position + moveVector, moveSpeed);

        }
    }
}
