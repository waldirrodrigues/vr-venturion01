﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenBehaviour : MonoBehaviour {

    public AudioSource doorSound;
    public GameObject pyreFire;
    
    public void Pyre()
    {
        pyreFire.SetActive(true);
    }

    public void PlayAudio()
    {
        doorSound.Play();
    }
    public void StopAudio()
    {
        doorSound.Stop();
    }
}
