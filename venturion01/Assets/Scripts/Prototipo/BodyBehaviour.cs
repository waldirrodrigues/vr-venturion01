﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyBehaviour : MonoBehaviour {

    public Vector3 initPoint;
    public Transform target;
    
	// Update is called once per frame
	void Update () {

        transform.position = new Vector3(target.position.x + initPoint.x, target.position.y + initPoint.y, target.position.z + initPoint.z);
		
	}
}
