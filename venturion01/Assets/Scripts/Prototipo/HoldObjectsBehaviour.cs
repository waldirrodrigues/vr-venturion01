﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoldObjectsBehaviour : MonoBehaviour {

    public bool inside;
    
	public void ThrowObject()
    {
        if (!inside)
        {
            transform.SetParent(null);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag=="Chair")
        {
            inside = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Chair")
        {
            inside = false;
        }
    }
}
