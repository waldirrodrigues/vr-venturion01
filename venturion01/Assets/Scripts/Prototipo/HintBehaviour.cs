﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HintBehaviour : MonoBehaviour {

    public bool activate;
    public float timeToHint;
    public Animator anim;
    public string triggerAnim;
    public AudioSource hintAudio;

    private float time;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update()
    {
        if(activate)
        {
            time += Time.deltaTime;

            if(time > timeToHint)
            {
                anim.SetTrigger(triggerAnim);
                hintAudio.Play();
            }
        }
	}

    public void Active()
    {
        activate = true;
    }
}
