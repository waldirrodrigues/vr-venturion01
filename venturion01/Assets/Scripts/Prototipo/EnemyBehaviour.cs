﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour {

    public Transform[] targets;
    public Transform target, baseTargets, player, playerPos;
    public float moveSpeed = 6, minDistance = 5, rotationSpeed = 1, timeToChangeTargetMin, timeToChangeTargetMax, percentage;
    public float bugSpeedNormal, bugSpeedAttack, propSpeedNormal, propSpeedAttack;

    public bool attack;
    public BugSeekBehaviour bugSeek;

    private float time, timeToChange;

    private void Start()
    {
        if (baseTargets != null)
        {
            targets = baseTargets.GetComponentsInChildren<Transform>();
        }
        
        target = targets[Random.Range(0, targets.Length)];

        timeToChange = Random.Range(timeToChangeTargetMin, timeToChangeTargetMax);

        player = GameObject.FindGameObjectWithTag("MainCamera").transform;
    }

    private void Update()
    {
        time += Time.deltaTime;

        Seek();
        
        if(time > timeToChange || (attack && Vector3.Distance(transform.position, target.position) < .3f))
        {

            if (Random.Range(0, 100) > percentage)
            {
                target = targets[Random.Range(0, targets.Length)];
                time = 0;
                timeToChange = Random.Range(timeToChangeTargetMin, timeToChangeTargetMax);
                minDistance = .1f;
                moveSpeed = propSpeedNormal;
                bugSeek.moveSpeed = bugSpeedNormal;
                attack = false;
            }

            else
            {
                Attack();
            }
        }
    }

    public void Seek() {

        Vector3 direction = target.position - transform.position;

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);

        //transform.LookAt(player);

        if (direction.magnitude > minDistance) {

            Vector3 moveVector = direction.normalized * moveSpeed * Time.deltaTime;

            transform.position += moveVector;

            //transform.position = Vector3.Lerp(transform.position, transform.position + moveVector, moveSpeed);

        }
    }

    public void Attack()
    {
        playerPos.position = player.position;

        target = playerPos;
        timeToChange = .2f;
        time = 0;
        minDistance = .1f;
        moveSpeed = propSpeedAttack;
        bugSeek.moveSpeed = bugSpeedAttack;
        attack = true;
    }
 
}
