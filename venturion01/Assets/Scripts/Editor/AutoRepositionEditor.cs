﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AutoReposition))]
[CanEditMultipleObjects]
public class AutoRepositionEditor : Editor {
    
    public override void OnInspectorGUI()
    {
        AutoReposition autoRepos = (AutoReposition)target;

        base.OnInspectorGUI();
        
        if(GUILayout.Button("Get Info"))
        {
            float value = autoRepos.GetZAngle();
            Debug.Log("Angle Z: " + value);
            value = autoRepos.GetYAngle();
            Debug.Log("Angle Y: " + value);
            Debug.Log("Lighthouse Right position: " + "( " + autoRepos.right.position.x + ", " + autoRepos.right.position.y + ", " + autoRepos.right.position.z + " )");
            Debug.Log("Lighthouse Left position: " + "( " + autoRepos.left.position.x + ", " + autoRepos.left.position.y + ", " + autoRepos.left.position.z + " )");
        }

        if(GUILayout.Button("Set Info"))
        {
            Vector3 autoPos = autoRepos.player.position;

            autoRepos.lighthouseRObj.position = autoRepos.lighthouseRight;
            autoRepos.lighthouseLObj.position = autoRepos.lighthouseLeft;

            autoRepos.newPos.position = autoRepos.lighthouseRight;

            autoRepos.player.position = autoPos;

            Debug.Log("Done");
        }
    }
}
