﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(Rigidbody))]
public class ProjectileBehaviour : MonoBehaviour {

    public float speed;

    private Rigidbody rig;

	// Use this for initialization
	void Start () {

        rig = GetComponent<Rigidbody>();

        rig.velocity = transform.up * speed;
	}
	
	// Update is called once per frame
	void Update ()
    {
        rig.velocity = transform.up * speed;

    }

    private void OnCollisionEnter(Collision collision)
    {
        LifeBehaviour life = collision.gameObject.GetComponent<LifeBehaviour>();
        if (life != null)
        {
            life.TakeDamage(10);
        }

        Destroy(gameObject);
    }
}
