﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ShotController))]
public class MonsterBehaviour : MonoBehaviour {

    public float shotTime;
    public Transform target;

    public float time;
    public ShotController shotCtrl;

	// Use this for initialization
	void Start () {
        
        shotCtrl = GetComponent<ShotController>();

        target = GameObject.FindGameObjectWithTag("MainCamera").transform;

	}
	
	// Update is called once per frame
	void Update () {

        transform.LookAt(target);

        time += Time.deltaTime;

        if (time > shotTime)
        {
            shotCtrl.Shot();
            time = 0;
        }
	}
}
