﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class TouchObjBehaviour : MonoBehaviour {

    public string tag;
    public List<GameObject> show;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == tag)
        {
            foreach (GameObject g in show)
            {
                g.SetActive(true);
            }
            //show.SetActive(show);
        }
    }
}
