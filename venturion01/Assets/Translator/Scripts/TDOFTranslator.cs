﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TDOFTranslator : MonoBehaviour {

#if DAYDREAM
    [Header("Click Button Down Event")]
    public UnityEvent clickButtonDownEvent;
    [Header("Click Button Up Event")]
    public UnityEvent clickButtonUpEvent;
    [Header("Hold Button Event")]
    public UnityEvent clickButtonEvent;
    [Header("Touch Button Down Event")]
    public UnityEvent touchDownEvent;
    [Header("Touch Button Up Event")]
    public UnityEvent touchUpEvent;
    [Header("App Button Down Event")]
    public UnityEvent appButtonDownEvent;
    [Header("App Button Up Event")]
    public UnityEvent appButtonUpEvent;
    [Header("Hold App Button Event")]
    public UnityEvent appButtonPressEvent;
    [Header("Home Button Down Event")]
    public UnityEvent homeButtonEvent;
    #endif

    public void Update()
    {
        VerifyGVR();
    }

    public void VerifyGVR()
    {

        #if DAYDREAM
        if(GvrController.AppButtonDown)
        {
            appButtonDownEvent.Invoke();
            return;
        }

        else if (GvrController.AppButtonUp)
        {
            appButtonUpEvent.Invoke();
            return;
        }
        
        else if (GvrController.AppButton)
        {
            appButtonPressEvent.Invoke();
            return;
        }

        else if (GvrController.TouchDown)
        {
            touchDownEvent.Invoke();
            return;
        }

        else if (GvrController.TouchUp)
        {
            touchUpEvent.Invoke();
            return;
        }

        else if (GvrController.ClickButtonDown)
        {
            clickButtonDownEvent.Invoke();
            return;
        }

        else if (GvrController.ClickButtonUp)
        {
            clickButtonUpEvent.Invoke();
            return;
        }

        else if (GvrController.ClickButton)
        {
            clickButtonEvent.Invoke();
            return;
        }

        else if (GvrController.HomeButtonDown)
        {
            homeButtonEvent.Invoke();
            return;
        }
        #endif

    }
}
