﻿#if STEAMVR
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR;

[RequireComponent(typeof(SteamVR_TrackedController))]
public class SteamVRTranslator : MonoBehaviour {

    SteamVR_TrackedController controller;
    
    public UnityEvent ButtonAPressEvent;
    public UnityEvent ButtonAUnpressEvent;
    public UnityEvent MenuPressEvent;
    public UnityEvent MenuUnpressEvent;
    public UnityEvent PadPressEvent;
    public UnityEvent PadUnpressEvent;
    public UnityEvent TriggerEvent;
    public UnityEvent UntriggerEvent;
    public UnityEvent GripEvent;
    public UnityEvent PadTouchEvent;
    public UnityEvent PadUntouchEvent;

    public VRControllerState_t controllerState;
    public event ClickedEventHandler APressed;
    public event ClickedEventHandler AUnpressed;
    private bool buttonA;

    private void Start()
    {
        controller = GetComponent<SteamVR_TrackedController>();

        LoadEvents();
    }
    
    private void Update()
    {
        var system = OpenVR.System;
        if (system != null && system.GetControllerState(controller.controllerIndex, ref controllerState, (uint)System.Runtime.InteropServices.Marshal.SizeOf(typeof(VRControllerState_t))))
        {
            ulong butA = controllerState.ulButtonPressed & (1UL << ((int)EVRButtonId.k_EButton_A));
            if (butA > 0L && !buttonA)
            {
                buttonA = true;
                ClickedEventArgs e;
                e.controllerIndex = controller.controllerIndex;
                e.flags = (uint)controllerState.ulButtonPressed;
                e.padX = controllerState.rAxis0.x;
                e.padY = controllerState.rAxis0.y;
                OnAPressed(e);
            }
            else if (butA == 0L && buttonA)
            {
                buttonA = false;
                ClickedEventArgs e;
                e.controllerIndex = controller.controllerIndex;
                e.flags = (uint)controllerState.ulButtonPressed;
                e.padX = controllerState.rAxis0.x;
                e.padY = controllerState.rAxis0.y;
                OnAUnpressed(e);
            }
        }
    }
    public virtual void OnAPressed(ClickedEventArgs e)
    {
        if (APressed != null)
            APressed(this, e);
    }
    public virtual void OnAUnpressed(ClickedEventArgs e)
    {
        if (AUnpressed != null)
            AUnpressed(this, e);
    }

    public void LoadEvents()
    {
        controller.MenuButtonClicked += Controller_MenuButtonClicked;
        controller.MenuButtonUnclicked += Controller_MenuButtonUnclicked;
        controller.Gripped += Controller_Gripped;
        APressed += SteamVRTranslator_APressed;
        AUnpressed += SteamVRTranslator_AUnpressed;
        controller.TriggerClicked += Controller_TriggerClicked;
        controller.TriggerUnclicked += Controller_TriggerUnclicked;
        controller.PadClicked += Controller_PadClicked;
        controller.PadUnclicked += Controller_PadUnclicked;
        controller.PadTouched += Controller_PadTouched;
        controller.PadUntouched += Controller_PadUntouched;
    }


    private void Controller_MenuButtonUnclicked(object sender, ClickedEventArgs e)
    {
        MenuUnpressEvent.Invoke();
    }
    private void Controller_MenuButtonClicked(object sender, ClickedEventArgs e)
    {
        MenuPressEvent.Invoke();
    }
    private void Controller_PadUntouched(object sender, ClickedEventArgs e)
    {
        PadUntouchEvent.Invoke();
    }
    private void Controller_PadTouched(object sender, ClickedEventArgs e)
    {
        PadTouchEvent.Invoke();
    }
    private void Controller_PadUnclicked(object sender, ClickedEventArgs e)
    {
        PadUnpressEvent.Invoke();
    }
    private void Controller_PadClicked(object sender, ClickedEventArgs e)
    {
        PadPressEvent.Invoke();
    }
    private void Controller_TriggerClicked(object sender, ClickedEventArgs e)
    {
        TriggerEvent.Invoke();
    }
    private void SteamVRTranslator_AUnpressed(object sender, ClickedEventArgs e)
    {
        ButtonAUnpressEvent.Invoke();
    }
    private void SteamVRTranslator_APressed(object sender, ClickedEventArgs e)
    {
        ButtonAPressEvent.Invoke();
    }
    private void Controller_Gripped(object sender, ClickedEventArgs e)
    {
        GripEvent.Invoke();
    }
    private void Controller_TriggerUnclicked(object sender, ClickedEventArgs e)
    {
        UntriggerEvent.Invoke();
    }
}
#endif