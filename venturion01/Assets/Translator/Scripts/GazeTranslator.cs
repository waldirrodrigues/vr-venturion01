﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;

public class GazeTranslator : MonoBehaviour {

    public UnityEvent GazeClickEvent;

#if GEARVR
    public UnityEvent SwipeUpEvent, SwipeDownEvent, SwipeLeftEvent, SwipeRightEvent, BackButtonEvent;
#endif

    void Awake()
    {
        if (GazeClickEvent == null)
            GazeClickEvent = new UnityEvent();
#if GEARVR
        if (SwipeUpEvent == null)
            SwipeUpEvent = new UnityEvent();
        if (SwipeDownEvent == null)
            SwipeDownEvent = new UnityEvent();
        if (SwipeLeftEvent == null)
            SwipeLeftEvent = new UnityEvent();
        if (SwipeRightEvent == null)
            SwipeRightEvent = new UnityEvent();
        
        OVRTouchpad.Update();
        OVRTouchpad.TouchHandler += OVRTouchpad_TouchHandler;
#endif

    }

    private void OVRTouchpad_TouchHandler(object sender, System.EventArgs e)
    {
#if GEARVR
        OVRTouchpad.TouchArgs touchArgs = (OVRTouchpad.TouchArgs)e;

        if(touchArgs.TouchType == OVRTouchpad.TouchEvent.SingleTap)
        {
            GazeClickEvent.Invoke();
        }

        else if(touchArgs.TouchType == OVRTouchpad.TouchEvent.Up)
        {
            SwipeUpEvent.Invoke();
        }

        else if (touchArgs.TouchType == OVRTouchpad.TouchEvent.Down)
        {
            SwipeDownEvent.Invoke();
        }

        else if (touchArgs.TouchType == OVRTouchpad.TouchEvent.Left)
        {
            SwipeLeftEvent.Invoke();
        }

        else if (touchArgs.TouchType == OVRTouchpad.TouchEvent.Right)
        {
            SwipeRightEvent.Invoke();
        }
#endif
    }

    private void Update()
    {
#if CARDBOARD
        if(Input.GetMouseButtonDown(0))
        {
            CardboardClick();
        }
#endif
#if GEARVR
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            BackClick();
        }
#endif
    }

    private void BackClick()
    {
#if GEARVR
        BackButtonEvent.Invoke();
#endif
    }

    private void CardboardClick()
    {
        GazeClickEvent.Invoke();
    }
}
