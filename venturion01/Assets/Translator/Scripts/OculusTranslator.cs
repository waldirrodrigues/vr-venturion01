﻿#if OCULUS
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OculusTranslator : MonoBehaviour {
    
    [Header("Button A")]
    public UnityEvent ButtonOneEvent;
    [Header("Button B")]
    public UnityEvent ButtonTwoEvent;
    [Header("Button X")]
    public UnityEvent ButtonThreeEvent;
    [Header("Button Y")]
    public UnityEvent ButtonFourEvent;

    [Header("Right Trigger")]
    public UnityEvent RightTriggerEvent;
    [Header("Left Trigger")]
    public UnityEvent LeftTriggerEvent;

    [Header("Right Grip")]
    public UnityEvent RightGripEvent;
    [Header("Left Grip")]
    public UnityEvent LeftGripEvent;

    [Header("Right Thumbstick Up")]
    public UnityEvent RightThumbstickUpEvent;
    [Header("Right Thumbstick Down")]
    public UnityEvent RightThumbstickDownEvent;
    [Header("Right Thumbstick Right")]
    public UnityEvent RightThumbstickRightEvent;
    [Header("Right Thumbstick Left")]
    public UnityEvent RightThumbstickLeftEvent;

    [Header("Left Thumbstick Up")]
    public UnityEvent LeftThumbstickUpEvent;
    [Header("Left Thumbstick Down")]
    public UnityEvent LeftThumbstickDownEvent;
    [Header("Left Thumbstick Right")]
    public UnityEvent LeftThumbstickRightEvent;
    [Header("Left Thumbstick Left")]
    public UnityEvent LeftThumbstickLeftEvent;

    [Header("Right Thumb Press")]
    public UnityEvent RightThumbEvent;
    [Header("Left Thumb Press")]
    public UnityEvent LeftThumbEvent;
    [Header("Start")]
    public UnityEvent StartEvent;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        VerifyOculusController();
		
	}

    public void VerifyOculusController()
    {
        if (OVRInput.GetDown(OVRInput.Button.One))
        {
            ButtonOneEvent.Invoke();
            return;
        }
        else if (OVRInput.GetDown(OVRInput.Button.Two))
        {
            ButtonTwoEvent.Invoke();
            return;
        }
        else if (OVRInput.GetDown(OVRInput.Button.Three))
        {
            ButtonThreeEvent.Invoke();
            return;
        }
        else if (OVRInput.GetDown(OVRInput.Button.Four))
        {
            ButtonFourEvent.Invoke();
            return;
        }
        else if (OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger))
        {
            RightTriggerEvent.Invoke();
            return;
        }
        else if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
        {
            LeftTriggerEvent.Invoke();
            return;
        }
        else if (OVRInput.GetDown(OVRInput.Button.SecondaryHandTrigger))
        {
            RightGripEvent.Invoke();
            return;
        }
        else if (OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger))
        {
            LeftGripEvent.Invoke();
            return;
        }
        else if (OVRInput.GetDown(OVRInput.Button.Start))
        {
            StartEvent.Invoke();
            return;
        }
        else if (OVRInput.GetDown(OVRInput.Button.SecondaryThumbstick))
        {
            RightThumbEvent.Invoke();
            return;
        }
        else if (OVRInput.GetDown(OVRInput.Button.PrimaryThumbstick))
        {
            LeftThumbEvent.Invoke();
            return;
        }
        else if (OVRInput.GetDown(OVRInput.Button.PrimaryThumbstickUp))
        {
            LeftThumbstickUpEvent.Invoke();
            return;
        }
        else if (OVRInput.GetDown(OVRInput.Button.PrimaryThumbstickDown))
        {
            LeftThumbstickDownEvent.Invoke();
            return;
        }
        else if (OVRInput.GetDown(OVRInput.Button.PrimaryThumbstickLeft))
        {
            LeftThumbstickRightEvent.Invoke();
            return;
        }
        else if (OVRInput.GetDown(OVRInput.Button.PrimaryThumbstickRight))
        {
            LeftThumbstickLeftEvent.Invoke();
            return;
        }
        else if (OVRInput.GetDown(OVRInput.Button.SecondaryThumbstickUp))
        {
            RightThumbstickUpEvent.Invoke();
            return;
        }
        else if (OVRInput.GetDown(OVRInput.Button.SecondaryThumbstickDown))
        {
            RightThumbstickDownEvent.Invoke();
            return;
        }
        else if (OVRInput.GetDown(OVRInput.Button.SecondaryThumbstickLeft))
        {
            RightThumbstickLeftEvent.Invoke();
            return;
        }
        else if (OVRInput.GetDown(OVRInput.Button.SecondaryThumbstickRight))
        {
            LeftThumbEvent.Invoke();
            return;
        }
    }

    public static Vector2 GetRightThumb()
    {
        return OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick, OVRInput.Controller.Touch);
    }
    public static Vector2 GetLeftThumb()
    {
        return OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.Touch);
    }
}
#endif