﻿#if STEAMVR
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SteamVRTranslator))]
[CanEditMultipleObjects]
public class SteamVRTranslatorEditor : Editor {

    private SerializedProperty buttonAPressProp;
    private SerializedProperty buttonAUnpressProp;

    private SerializedProperty menuPressProp;
    private SerializedProperty menuUnpressProp;

    private SerializedProperty padPressProp;
    private SerializedProperty padUnpressProp;
    
    private SerializedProperty padTouchProp;
    private SerializedProperty padUntouchProp;

    private SerializedProperty triggerProp;
    private SerializedProperty untriggerProp;
    private SerializedProperty gripProp;
    
    public override void OnInspectorGUI()
    {
        SteamVRTranslator steamvrTrans = (SteamVRTranslator)target;

        ShowEvents(steamvrTrans);

        if (GUILayout.Button("Adicionar Evento"))
        {
            GenericMenu menu = new GenericMenu();
            menu.AddItem(new GUIContent("Press Button A"), buttonAPressProp != null, ShowPressAGUI);
            menu.AddItem(new GUIContent("Unpress Button A"), buttonAUnpressProp != null, ShowUnpressAGUI);

            menu.AddItem(new GUIContent("Press Menu"), menuPressProp != null, ShowPressMenuGUI);
            menu.AddItem(new GUIContent("Unpress Menu"), menuUnpressProp != null, ShowUnpressMenuGUI);

            menu.AddItem(new GUIContent("Press Pad"), padPressProp != null, ShowPressPadGUI);
            menu.AddItem(new GUIContent("Unpress Pad"), padUnpressProp != null, ShowUnpressPadGUI);

            menu.AddItem(new GUIContent("Touch Pad"), padTouchProp != null, ShowTouchPadGUI);
            menu.AddItem(new GUIContent("Untouch Pad"), padUntouchProp != null, ShowUntouchPadGUI);

            menu.AddItem(new GUIContent("Press Trigger"), triggerProp != null, ShowPressTriggerGUI);
            menu.AddItem(new GUIContent("Press Untrigger"), untriggerProp != null, ShowPressUntriggerGUI);
            menu.AddItem(new GUIContent("Press Grip"), gripProp != null, ShowPressGripGUI);

            menu.ShowAsContext();
        }
    }

    private void ShowPressAGUI()
    {
        buttonAPressProp = serializedObject.FindProperty("ButtonAPressEvent");
    }
    private void ShowUnpressAGUI()
    {
        buttonAUnpressProp = serializedObject.FindProperty("ButtonAUnpressEvent");
    }

    private void ShowPressMenuGUI()
    {
        menuPressProp = serializedObject.FindProperty("MenuPressEvent");
    }
    private void ShowUnpressMenuGUI()
    {
        menuUnpressProp = serializedObject.FindProperty("MenuUnpressEvent");
    }

    private void ShowPressPadGUI()
    {
        padPressProp = serializedObject.FindProperty("PadPressEvent");
    }
    private void ShowUnpressPadGUI()
    {
        padUnpressProp = serializedObject.FindProperty("PadUnpressEvent");
    }
    
    private void ShowTouchPadGUI()
    {
        padTouchProp = serializedObject.FindProperty("PadTouchEvent");
    }
    private void ShowUntouchPadGUI()
    {
        padUntouchProp = serializedObject.FindProperty("PadUntouchEvent");
    }

    private void ShowPressTriggerGUI()
    {
        triggerProp = serializedObject.FindProperty("TriggerEvent");
    }
    private void ShowPressUntriggerGUI()
    {
        untriggerProp = serializedObject.FindProperty("UntriggerEvent");
    }
    private void ShowPressGripGUI()
    {
        gripProp = serializedObject.FindProperty("GripEvent");
    }

    private void ShowEvents(SteamVRTranslator steamvrTrans)
    {
        if (steamvrTrans.ButtonAPressEvent.GetPersistentEventCount() > 0 || buttonAPressProp != null)
        {
            buttonAPressProp = serializedObject.FindProperty("ButtonAPressEvent");
            EditorGUILayout.PropertyField(buttonAPressProp);
            serializedObject.ApplyModifiedProperties();
        }
        if (steamvrTrans.ButtonAUnpressEvent.GetPersistentEventCount() > 0 || buttonAUnpressProp != null)
        {
            buttonAUnpressProp = serializedObject.FindProperty("ButtonAUnpressEvent");
            EditorGUILayout.PropertyField(buttonAUnpressProp);
            serializedObject.ApplyModifiedProperties();
        }
        
        if (steamvrTrans.MenuPressEvent.GetPersistentEventCount() > 0 || menuPressProp != null)
        {
            menuPressProp = serializedObject.FindProperty("MenuPressEvent");
            EditorGUILayout.PropertyField(menuPressProp);
            serializedObject.ApplyModifiedProperties();
        }
        if (steamvrTrans.MenuUnpressEvent.GetPersistentEventCount() > 0 || menuUnpressProp != null)
        {
            menuUnpressProp = serializedObject.FindProperty("MenuUnpressEvent");
            EditorGUILayout.PropertyField(menuUnpressProp);
            serializedObject.ApplyModifiedProperties();
        }

        if (steamvrTrans.PadPressEvent.GetPersistentEventCount() > 0 || padPressProp != null)
        {
            padPressProp = serializedObject.FindProperty("PadPressEvent");
            EditorGUILayout.PropertyField(padPressProp);
            serializedObject.ApplyModifiedProperties();
        }
        if (steamvrTrans.PadUnpressEvent.GetPersistentEventCount() > 0 || padUnpressProp != null)
        {
            padUnpressProp = serializedObject.FindProperty("PadUnpressEvent");
            EditorGUILayout.PropertyField(padUnpressProp);
            serializedObject.ApplyModifiedProperties();
        }

        if (steamvrTrans.TriggerEvent.GetPersistentEventCount() > 0 || triggerProp != null)
        {
            triggerProp = serializedObject.FindProperty("TriggerEvent");
            EditorGUILayout.PropertyField(triggerProp);
            serializedObject.ApplyModifiedProperties();
        }
        if (steamvrTrans.TriggerEvent.GetPersistentEventCount() > 0 || untriggerProp != null)
        {
            untriggerProp = serializedObject.FindProperty("UntriggerEvent");
            EditorGUILayout.PropertyField(untriggerProp);
            serializedObject.ApplyModifiedProperties();
        }
        if (steamvrTrans.GripEvent.GetPersistentEventCount() > 0 || gripProp != null)
        {
            gripProp = serializedObject.FindProperty("GripEvent");
            EditorGUILayout.PropertyField(gripProp);
            serializedObject.ApplyModifiedProperties();
        }

        if (steamvrTrans.PadTouchEvent.GetPersistentEventCount() > 0 || padTouchProp != null)
        {
            padTouchProp = serializedObject.FindProperty("PadTouchEvent");
            EditorGUILayout.PropertyField(padTouchProp);
            serializedObject.ApplyModifiedProperties();
        }
        if (steamvrTrans.PadUntouchEvent.GetPersistentEventCount() > 0 || padUntouchProp != null)
        {
            padUntouchProp = serializedObject.FindProperty("PadUntouchEvent");
            EditorGUILayout.PropertyField(padUntouchProp);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
#endif