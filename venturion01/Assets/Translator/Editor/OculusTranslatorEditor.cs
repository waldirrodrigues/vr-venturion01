﻿#if OCULUS
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Events;
using System;

[CustomEditor(typeof(OculusTranslator))]
[CanEditMultipleObjects]
public class OculusTranslatorEditor : Editor
{
    private SerializedProperty buttonOneProp;
    private SerializedProperty buttonTwoProp;
    private SerializedProperty buttonThreeProp;
    private SerializedProperty buttonFourProp;

    private SerializedProperty rightTriggerProp;
    private SerializedProperty leftTriggerProp;

    private SerializedProperty rightGripProp;
    private SerializedProperty leftGripProp;

    private SerializedProperty rightThumbstickUpProp;
    private SerializedProperty rightThumbstickDownProp;
    private SerializedProperty rightThumbstickLeftProp;
    private SerializedProperty rightThumbstickRightProp;

    private SerializedProperty leftThumbstickUpProp;
    private SerializedProperty leftThumbstickDownProp;
    private SerializedProperty leftThumbstickLeftProp;
    private SerializedProperty leftThumbstickRightProp;

    private SerializedProperty rightThumbProp;
    private SerializedProperty leftThumbProp;
    private SerializedProperty startProp;

    public override void OnInspectorGUI() 
    {
        OculusTranslator oculusTrans = (OculusTranslator)target;

        ShowEvents(oculusTrans);

        if (GUILayout.Button("Adicionar Evento"))
        {
            GenericMenu menu = new GenericMenu();
            menu.AddItem(new GUIContent("Press Button A"), buttonOneProp != null, ShowButtonAGUI);
            menu.AddItem(new GUIContent("Press Button B"), buttonTwoProp != null, ShowButtonBGUI);
            menu.AddItem(new GUIContent("Press Button X"), buttonThreeProp != null, ShowButtonXGUI);
            menu.AddItem(new GUIContent("Press Button Y"), buttonFourProp != null, ShowButtonYGUI);

            menu.AddItem(new GUIContent("Press Right Trigger"), rightTriggerProp != null, ShowRightTriggerGUI);
            menu.AddItem(new GUIContent("Press Left Trigger"), leftTriggerProp != null, ShowLeftTriggerGUI);

            menu.AddItem(new GUIContent("Press Right Grip"), rightGripProp != null, ShowRightGripGUI);
            menu.AddItem(new GUIContent("Press Left Grip"), leftGripProp != null, ShowLeftGripGUI);

            menu.AddItem(new GUIContent("Right Thumbstick Up"), rightThumbstickUpProp != null, RightThumbUpGUI);
            menu.AddItem(new GUIContent("Right Thumbstick Down"), rightThumbstickDownProp != null, RightThumbDownGUI);
            menu.AddItem(new GUIContent("Right Thumbstick Left"), rightThumbstickLeftProp != null, RightThumbLeftGUI);
            menu.AddItem(new GUIContent("Right Thumbstick Right"), rightThumbstickRightProp != null, RightThumbRightGUI);

            menu.AddItem(new GUIContent("Left Thumbstick Up"), leftThumbstickUpProp != null, LeftThumbUpGUI);
            menu.AddItem(new GUIContent("Left Thumbstick Down"), leftThumbstickDownProp != null, LeftThumbDownGUI);
            menu.AddItem(new GUIContent("Left Thumbstick Left"), leftThumbstickLeftProp != null, LeftThumbLeftGUI);
            menu.AddItem(new GUIContent("Left Thumbstick Right"), leftThumbstickRightProp != null, LeftThumbRightGUI);

            menu.AddItem(new GUIContent("Press Right Thumbstick"), rightThumbProp != null, RightThumbPressGUI);
            menu.AddItem(new GUIContent("Press Left Thumbstick"), leftThumbProp != null, LeftThumbPressGUI);
            menu.AddItem(new GUIContent("Press Start"), startProp != null, PressStartGUI);

            menu.ShowAsContext();
        }
    }

    void ShowButtonAGUI()
    {
        buttonOneProp = serializedObject.FindProperty("ButtonOneEvent");
    }
    void ShowButtonBGUI()
    {
        buttonTwoProp = serializedObject.FindProperty("ButtonTwoEvent");
    }
    void ShowButtonXGUI()
    {
        buttonThreeProp = serializedObject.FindProperty("ButtonThreeEvent");
    }
    void ShowButtonYGUI()
    {
        buttonFourProp = serializedObject.FindProperty("ButtonFourEvent");
    }
    
    void ShowRightTriggerGUI()
    {
        rightTriggerProp = serializedObject.FindProperty("RightTriggerEvent");
    }
    void ShowLeftTriggerGUI()
    {
        leftTriggerProp = serializedObject.FindProperty("LeftTriggerEvent");
    }

    void ShowRightGripGUI()
    {
        rightGripProp = serializedObject.FindProperty("RightGripEvent");
    }
    void ShowLeftGripGUI()
    {
        leftGripProp = serializedObject.FindProperty("LeftGripEvent");
    }

    void RightThumbUpGUI()
    {
        rightThumbstickUpProp = serializedObject.FindProperty("RightThumbstickUpEvent");
    }
    void RightThumbDownGUI()
    {
        rightThumbstickDownProp = serializedObject.FindProperty("RightThumbstickDownEvent");
    }
    void RightThumbLeftGUI()
    {
        rightThumbstickLeftProp = serializedObject.FindProperty("RightThumbstickLeftEvent");
    }
    void RightThumbRightGUI()
    {
        rightThumbstickRightProp = serializedObject.FindProperty("RightThumbstickRightEvent");
    }

    void LeftThumbUpGUI()
    {
        leftThumbstickUpProp = serializedObject.FindProperty("LeftThumbstickUpEvent");
    }
    void LeftThumbDownGUI()
    {
        leftThumbstickDownProp = serializedObject.FindProperty("LeftThumbstickDownEvent");
    }
    void LeftThumbLeftGUI()
    {
        leftThumbstickLeftProp = serializedObject.FindProperty("LeftThumbstickLeftEvent");
    }
    void LeftThumbRightGUI()
    {
        leftThumbstickRightProp = serializedObject.FindProperty("LeftThumbstickRightEvent");
    }

    void RightThumbPressGUI()
    {
        rightThumbProp = serializedObject.FindProperty("RightThumbEvent");
    }
    void LeftThumbPressGUI()
    {
        leftThumbProp = serializedObject.FindProperty("LeftThumbEvent");
    }
    void PressStartGUI()
    {
        startProp = serializedObject.FindProperty("StartEvent");
    }

    void ShowEvents(OculusTranslator oculusTrans)
    {
        #region Button Events
        if (oculusTrans.ButtonOneEvent.GetPersistentEventCount() > 0 || buttonOneProp != null)
        {
            buttonOneProp = serializedObject.FindProperty("ButtonOneEvent");
            EditorGUILayout.PropertyField(buttonOneProp);
            serializedObject.ApplyModifiedProperties();
        }
        if (oculusTrans.ButtonTwoEvent.GetPersistentEventCount() > 0 || buttonTwoProp != null)
        {
            buttonTwoProp = serializedObject.FindProperty("ButtonTwoEvent");
            EditorGUILayout.PropertyField(buttonTwoProp);
            serializedObject.ApplyModifiedProperties();
        }
        if (oculusTrans.ButtonThreeEvent.GetPersistentEventCount() > 0 || buttonThreeProp != null)
        {
            buttonThreeProp = serializedObject.FindProperty("ButtonThreeEvent");
            EditorGUILayout.PropertyField(buttonThreeProp);
            serializedObject.ApplyModifiedProperties();
        }
        if (oculusTrans.ButtonFourEvent.GetPersistentEventCount() > 0 || buttonFourProp != null)
        {
            buttonFourProp = serializedObject.FindProperty("ButtonFourEvent");
            EditorGUILayout.PropertyField(buttonFourProp);
            serializedObject.ApplyModifiedProperties();
        }
        #endregion

        #region Trigger Events
        if (oculusTrans.RightTriggerEvent.GetPersistentEventCount() > 0 || rightTriggerProp != null)
        {
            rightTriggerProp = serializedObject.FindProperty("RightTriggerEvent");
            EditorGUILayout.PropertyField(rightTriggerProp);
            serializedObject.ApplyModifiedProperties();
        }
        if (oculusTrans.LeftTriggerEvent.GetPersistentEventCount() > 0 || leftTriggerProp != null)
        {
            leftTriggerProp = serializedObject.FindProperty("LeftTriggerEvent");
            EditorGUILayout.PropertyField(leftTriggerProp);
            serializedObject.ApplyModifiedProperties();
        }
        #endregion

        #region Grip Events
        if (oculusTrans.RightGripEvent.GetPersistentEventCount() > 0 || rightGripProp != null)
        {
            rightGripProp = serializedObject.FindProperty("RightGripEvent");
            EditorGUILayout.PropertyField(rightGripProp);
            serializedObject.ApplyModifiedProperties();
        }
        if (oculusTrans.LeftGripEvent.GetPersistentEventCount() > 0 || leftGripProp != null)
        {
            leftGripProp = serializedObject.FindProperty("LeftGripEvent");
            EditorGUILayout.PropertyField(leftGripProp);
            serializedObject.ApplyModifiedProperties();
        }
        #endregion

        #region Right Thumbstick Events
        if (oculusTrans.RightThumbstickUpEvent.GetPersistentEventCount() > 0 || rightThumbstickUpProp != null)
        {
            rightThumbstickUpProp = serializedObject.FindProperty("RightThumbstickUpEvent");
            EditorGUILayout.PropertyField(rightThumbstickUpProp);
            serializedObject.ApplyModifiedProperties();
        }
        if (oculusTrans.RightThumbstickDownEvent.GetPersistentEventCount() > 0 || rightThumbstickDownProp != null)
        {
            rightThumbstickDownProp = serializedObject.FindProperty("RightThumbstickDownEvent");
            EditorGUILayout.PropertyField(rightThumbstickDownProp);
            serializedObject.ApplyModifiedProperties();
        }
        if (oculusTrans.RightThumbstickLeftEvent.GetPersistentEventCount() > 0 || rightThumbstickLeftProp != null)
        {
            rightThumbstickLeftProp = serializedObject.FindProperty("RightThumbstickLeftEvent");
            EditorGUILayout.PropertyField(rightThumbstickLeftProp);
            serializedObject.ApplyModifiedProperties();
        }
        if (oculusTrans.RightThumbstickRightEvent.GetPersistentEventCount() > 0 || rightThumbstickRightProp != null)
        {
            rightThumbstickRightProp = serializedObject.FindProperty("RightThumbstickRightEvent");
            EditorGUILayout.PropertyField(rightThumbstickRightProp);
            serializedObject.ApplyModifiedProperties();
        }
        #endregion

        #region Left Thumbstick Events
        if (oculusTrans.LeftThumbstickUpEvent.GetPersistentEventCount() > 0 || leftThumbstickUpProp != null)
        {
            leftThumbstickUpProp = serializedObject.FindProperty("LeftThumbstickUpEvent");
            EditorGUILayout.PropertyField(leftThumbstickUpProp);
            serializedObject.ApplyModifiedProperties();
        }
        if (oculusTrans.LeftThumbstickDownEvent.GetPersistentEventCount() > 0 || leftThumbstickDownProp != null)
        {
            leftThumbstickDownProp = serializedObject.FindProperty("LeftThumbstickDownEvent");
            EditorGUILayout.PropertyField(leftThumbstickDownProp);
            serializedObject.ApplyModifiedProperties();
        }
        if (oculusTrans.LeftThumbstickLeftEvent.GetPersistentEventCount() > 0 || leftThumbstickLeftProp != null)
        {
            leftThumbstickLeftProp = serializedObject.FindProperty("LeftThumbstickLeftEvent");
            EditorGUILayout.PropertyField(leftThumbstickLeftProp);
            serializedObject.ApplyModifiedProperties();
        }
        if (oculusTrans.LeftThumbstickRightEvent.GetPersistentEventCount() > 0 || leftThumbstickRightProp != null)
        {
            leftThumbstickRightProp = serializedObject.FindProperty("LeftThumbstickRightEvent");
            EditorGUILayout.PropertyField(leftThumbstickRightProp);
            serializedObject.ApplyModifiedProperties();
        }
        #endregion

        #region Thumbstick Press Events
        if (oculusTrans.RightThumbEvent.GetPersistentEventCount() > 0 || rightThumbProp != null)
        {
            rightThumbProp = serializedObject.FindProperty("RightThumbEvent");
            EditorGUILayout.PropertyField(rightThumbProp);
            serializedObject.ApplyModifiedProperties();
        }
        if (oculusTrans.LeftThumbEvent.GetPersistentEventCount() > 0 || leftThumbProp != null)
        {
            leftThumbProp = serializedObject.FindProperty("LeftThumbEvent");
            EditorGUILayout.PropertyField(leftThumbProp);
            serializedObject.ApplyModifiedProperties();
        }
        #endregion

        #region Start Event
        if (oculusTrans.StartEvent.GetPersistentEventCount() > 0 || startProp != null)
        {
            startProp = serializedObject.FindProperty("StartEvent");
            EditorGUILayout.PropertyField(startProp);
            serializedObject.ApplyModifiedProperties();
        }
        #endregion
    }
}
#endif