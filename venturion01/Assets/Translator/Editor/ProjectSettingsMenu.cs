﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class ProjectSettingsMenu
{

    [MenuItem("Switch Platform/Switch To GearVR")]
    public static void GearVR()
    {
        string[] str = new string[1];
        str[0] = "Oculus";

        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android);
        PlayerSettings.Android.minSdkVersion = AndroidSdkVersions.AndroidApiLevel21;
        PlayerSettings.virtualRealitySupported = true;
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, "GEARVR");
        QualitySettings.pixelLightCount = 1;
        QualitySettings.antiAliasing = 2;
        QualitySettings.anisotropicFiltering = AnisotropicFiltering.Enable;
        QualitySettings.masterTextureLimit = 0;
        QualitySettings.softParticles = false;
        QualitySettings.realtimeReflectionProbes = true;
        QualitySettings.billboardsFaceCameraPosition = true;


        UnityEditorInternal.VR.VREditor.SetVREnabledDevicesOnTargetGroup(BuildTargetGroup.Android, str);
    }

    [MenuItem("Switch Platform/Switch To Cardboard")]
    public static void Cardboard()
    {
        string[] str = new string[1];
        str[0] = "cardboard";

        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android);
        PlayerSettings.Android.minSdkVersion = AndroidSdkVersions.AndroidApiLevel19;
        PlayerSettings.virtualRealitySupported = true;
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, "CARDBOARD");
        QualitySettings.pixelLightCount = 1;
        QualitySettings.antiAliasing = 2;
        QualitySettings.anisotropicFiltering = AnisotropicFiltering.Enable;
        QualitySettings.masterTextureLimit = 0;
        QualitySettings.softParticles = false;
        QualitySettings.realtimeReflectionProbes = true;
        QualitySettings.billboardsFaceCameraPosition = true;


        UnityEditorInternal.VR.VREditor.SetVREnabledDevicesOnTargetGroup(BuildTargetGroup.Android, str);
        //DeleteOVRFiles();
    }

    [MenuItem("Switch Platform/Switch To Daydream")]
    public static void Daydream()
    {
        string[] str = new string[1];
        str[0] = "daydream";

        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android);
        PlayerSettings.Android.minSdkVersion = AndroidSdkVersions.AndroidApiLevel24;
        PlayerSettings.virtualRealitySupported = true;
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, "DAYDREAM");
        QualitySettings.pixelLightCount = 1;
        QualitySettings.antiAliasing = 2;
        QualitySettings.anisotropicFiltering = AnisotropicFiltering.Enable;
        QualitySettings.masterTextureLimit = 0;
        QualitySettings.softParticles = false;
        QualitySettings.realtimeReflectionProbes = true;
        QualitySettings.billboardsFaceCameraPosition = true;


        UnityEditorInternal.VR.VREditor.SetVREnabledDevicesOnTargetGroup(BuildTargetGroup.Android, str);
        UnityEditorInternal.VR.VREditor.SetVREnabledDevicesOnTargetGroup(BuildTargetGroup.Android, str);
    }

    [MenuItem("Switch Platform/Switch To Oculus Rift")]
    public static void OculusRift()
    {
        string[] str = new string[1];
        str[0] = "Oculus";

        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows);

        PlayerSettings.virtualRealitySupported = true;
        PlayerSettings.MTRendering = true;
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone, "OCULUS");
        QualitySettings.pixelLightCount = 3;
        QualitySettings.antiAliasing = 2;
        QualitySettings.anisotropicFiltering = AnisotropicFiltering.Enable;
        QualitySettings.masterTextureLimit = 0;
        QualitySettings.softParticles = false;
        QualitySettings.realtimeReflectionProbes = true;
        QualitySettings.billboardsFaceCameraPosition = true;


        UnityEditorInternal.VR.VREditor.SetVREnabledDevicesOnTargetGroup(BuildTargetGroup.Android, str);
    }

    [MenuItem("Switch Platform/Switch To SteamVR")]
    public static void SteamVR()
    {
        string[] str = new string[1];
        str[0] = "OpenVR";

        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows);

        PlayerSettings.virtualRealitySupported = true;
        PlayerSettings.MTRendering = true;
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone, "STEAMVR");
        QualitySettings.pixelLightCount = 3;
        QualitySettings.antiAliasing = 2;
        QualitySettings.anisotropicFiltering = AnisotropicFiltering.Enable;
        QualitySettings.masterTextureLimit = 0;
        QualitySettings.softParticles = false;
        QualitySettings.realtimeReflectionProbes = true;
        QualitySettings.billboardsFaceCameraPosition = true;


        UnityEditorInternal.VR.VREditor.SetVREnabledDevicesOnTargetGroup(BuildTargetGroup.Android, str);
    }

    public static void DeleteOVRFiles()
    {
        var dir = new DirectoryInfo(Directory.GetCurrentDirectory() + "\\Assets\\OVR");
        dir.Delete(true);
    }
}
